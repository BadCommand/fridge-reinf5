package fridge;

public class Fridge {

	private int numbDrinks = 0;
	private double amountOfMilk = 0.0;

	public Fridge(int numbDrinks, double amountOfMilk) {
		this.numbDrinks = numbDrinks;
		this.amountOfMilk = amountOfMilk;
	}

	public int getNumberOfDrinks() {

		return numbDrinks;
	}

	public double getLitersOfMilk() {

		return amountOfMilk;
	}

	public String takeADrink() {
		String outputDrinks = "";
		if (numbDrinks >= 1) {
			numbDrinks -= 1;
			outputDrinks = "Here is your drink.";
		} else {
			outputDrinks = "Not enough drinks!";
		}

		return outputDrinks;
	}

	public String takeMilk(double litersOfMilk) {

		String outputMilk = "";
		if (amountOfMilk >= litersOfMilk) {
			amountOfMilk -= litersOfMilk;
			outputMilk = "Here is your milk.";
		} else {
			outputMilk = "Not enough milk!";
		}
		return outputMilk;
	}

	public void fillMilk(double litersOfMilk) {

		amountOfMilk += litersOfMilk;
	}

	public void fillDrinks(int number) {

		numbDrinks += number;
	}
}
